import json
import threading
import time

from telegram import *
from telegram.ext import *


class telegramBot:
    def __init__(self):
        with open('telegramToken.json', 'r') as file:
            self.token = json.load(file)[0]

        with open('telegramData.json', 'r') as file:
            self.data = json.load(file)
        self.updater = Updater(token=self.token, use_context=False)
        updateDataPolling = threading.Thread(target=self.updateDataPolling)
        updateDataPolling.start()
        dispatcher = self.updater.dispatcher
        dispatcher.add_handler(ConversationHandler(entry_points=[CommandHandler('start', self.start)],
                                                   states={0: [CallbackQueryHandler(self.selectCategory)]},
                                                   fallbacks=[CommandHandler('start', self.start)],
                                                   conversation_timeout=30 * 24 * 60 * 60))

        dispatcher.add_handler(ConversationHandler(entry_points=[CommandHandler('changeTopics', self.changeTopics)],
                                                   states={0: [CallbackQueryHandler(self.editTopics)],
                                                           1: [MessageHandler(Filters.regex('^^'), self.addCategory)]},
                                                   fallbacks=[CommandHandler('start', self.start),
                                                              CommandHandler('changeTopics', self.changeTopics)],
                                                   conversation_timeout=30 * 24 * 60 * 60))

    def getCategoryKeyboard(self):
        keyboard = []
        for keyboardLine in range(0, len(self.data['categoryList']) - len(self.data['categoryList']) % 2, 2):
            keyboard.append([InlineKeyboardButton(self.data['categoryList'][keyboardLine],
                                                  callback_data=self.data['categoryList'][keyboardLine]),
                             InlineKeyboardButton(self.data['categoryList'][keyboardLine + 1],
                                                  callback_data=self.data['categoryList'][keyboardLine + 1])])
        if len(self.data['categoryList']) % 2:
            keyboard.append([InlineKeyboardButton(self.data['categoryList'][-1],   callback_data=self.data['categoryList'][-1])])
        reply_markup = InlineKeyboardMarkup(keyboard)
        return reply_markup

    def start(self, bot, update):
        bot.send_video(chat_id=update.message.chat_id, video=open('novyRyazansky.MP4', 'rb'), supports_streaming=True)
        self.send_message(update,
                          "Выберете категорию",
                          reply_markup=self.getCategoryKeyboard()
                          )
        return 0

    def changeTopics(self, bot, update):
        if update.message.chat_id != 252404343 and update.message.chat_id != 688079546:
            return -1
        self.send_message(update,
                          "Test",
                          reply_markup=InlineKeyboardMarkup(
                              [[InlineKeyboardButton('удалить котегорию', callback_data='deleteTopic')],
                               [InlineKeyboardButton('добавить котегорию', callback_data='addTopic')]])
                          )
        return 0

    def editTopics(self, bot, update):
        category = update.callback_query.data
        if category == 'deleteTopic':
            bot.send_message(chat_id=update.callback_query.message.chat_id,
                             text=f"Test", reply_markup=self.getCategoryKeyboard())
            return 0

        elif category == 'addTopic':
            bot.send_message(chat_id=update.callback_query.message.chat_id, text='новая категория')
            return 1
        else:
            del self.data["categoryChats"][category]
            self.data['categoryList'].pop(self.data['categoryList'].index(category))
            with open('telegramData.json', 'w') as file:
                json.dump(self.data, file)
            bot.send_message(chat_id=update.callback_query.message.chat_id, text='категория удалена')
        return -1

    def addCategory(self, bot, update):
        category = update.message.text
        category = category.split('\n')
        self.data['categoryList'].append(category[0])
        if len(category) > 2:
            self.data['categoryChats'][category[0]] = category[1::]
        else:
            self.data['categoryChats'][category[0]] = list(category[1::])

        self.send_message(update, 'категория добавлена')
        with open('telegramData.json', 'w') as file:
            json.dump(self.data, file)
        return -1

    def send_message(self, update, msg, **kwargs):
        self.updater.bot.send_message(chat_id=update.message.chat_id, text=msg, **kwargs)

    def selectCategory(self, bot, update):
        category = update.callback_query.data
        if category == 'mainMenu':
            reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton('главное меню', callback_data='mainMenu')]])
            bot.send_message(chat_id=update.callback_query.message.chat_id,
                             text=f"главное меню", reply_markup=self.getCategoryKeyboard()),
            # update.message.send_message(
            #     "Выберете категорию",
            #     reply_markup=self.getCategoryKeyboard()
            # )
            return 0
        if category in self.data['categoryChats'].keys():
            for chat in self.data['categoryChats'][category]:
                bot.send_message(chat_id=update.callback_query.message.chat_id, text=chat)
            # self.send_message(update, f"⬆️выше показаны все чаты из {category} ⬆️")
            reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton('главное меню', callback_data='mainMenu')]])
            bot.send_message(chat_id=update.callback_query.message.chat_id,
                             text=f"выше показаны все чаты из {category}", reply_markup=reply_markup)
            return 0
        else:
            return -1

    def updateData(self):
        with open('telegramData.json', 'r') as file:
            self.data = json.load(file)

    def updateDataPolling(self):
        while True:
            time.sleep(5 * 60)
            self.updateData()

    def run(self):
        self.updater.start_polling()

if __name__ == '__main__':
    bot = telegramBot()
    bot.run()
